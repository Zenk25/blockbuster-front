import { Component, OnInit } from '@angular/core';
import { Juego } from './juego';
import { JuegoService } from './juego.service';
import { AlertService } from '../alert/alert.service';

@Component({
  selector: 'app-juegos',
  templateUrl: './juegos.component.html',
  styleUrls: []
})
export class JuegosComponent implements OnInit {
  juegos: Juego[] = [];
  showId :boolean;

  constructor(private juegoService : JuegoService, private alertService : AlertService) { }

  ngOnInit(): void {
    this.refreshJuegos();
  }

  refreshJuegos(): void{
    this.juegoService.getJuegos().subscribe(
      juegos => this.juegos = juegos
    );
  }

  switchId() :void {
    this.showId = !this.showId;
  }

  delete(id: number): void{
    if(confirm(`¿Esta seguro que desea elminar el juego con ID: ${id}`)){
     this.juegoService.deleteJuego(id).subscribe(response => {
      this.alertService.success(`Se ha eliminado correctamente el juego con ID: ${id}`, {autoClose: true});
      this.refreshJuegos();
     });
    }
  }
}
