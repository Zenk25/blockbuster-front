import { Component, OnInit } from '@angular/core';
import { Juego } from './juego';
import { JuegoService } from './juego.service';
import { AlertService } from '../alert/alert.service';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-juegoform',
  templateUrl: './form.component.html',
  styleUrls: []
})
export class JuegoFormComponent implements OnInit {

  juego : Juego = new Juego();
  categorias : any[] = [{value:'SHOOTER', title:'Shooter'},{value: 'MOBA',title:'Moba'},{value: 'RPG',title:'RPG'},{value: 'ROGUELIKE',title:'Roguelike'},{value: 'METROIDVANIA',title:'Metroidvania'}];
  title : string;

  constructor(private juegoService: JuegoService, private alertService: AlertService, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.loadJuego();
  }

  create(): void{
    this.juegoService.createJuego(this.juego).subscribe(
      juego => {
        this.alertService.success(`Se ha creado correctamente el juego "${juego.titulo}" con ID: ${juego.idJuego}`)
        this.router.navigate(['/juegos']);
    }
    )
  }

  loadJuego() : void{
    this.activatedRoute.params.subscribe(params => {
      let id = params['id'];
      if(id){
        this.title = 'Editar Juego';
        this.juegoService.getJuego(id).subscribe(
          juego => this.juego = juego
        )
      }else{
        this.title = 'Crear Juego';
      }
    })
  }

  update(): void{
    this.juegoService.updateJuego(this.juego).subscribe(
      juego => {
        this.alertService.success(`Se ha actualizado correctamente el juego "${juego.titulo}" con ID: ${juego.idJuego}`)
        this.router.navigate(['/juegos']);
    }
    )
  }

  delete() : void{
    
  }
}
