import { Juego } from "./juego";

export const JUEGOS: Juego[] = [
    {idJuego : 1,titulo : 'Jak & Daxter', fechaLanzamiento : '2003-10-03', categoria : 'Plataforma', pegi: 12},
    {idJuego : 2,titulo : 'Little Nightmares', fechaLanzamiento : '2017-04-28', categoria : 'Rompecabezas', pegi: 16},
    {idJuego : 3,titulo : 'Alice: Madness Returns ', fechaLanzamiento : '2011-06-14', categoria : 'Aventura', pegi: 18},
    {idJuego : 4,titulo : 'Bioshock: Infinite', fechaLanzamiento : '2011-09-04', categoria : 'FPS', pegi: 18},
    {idJuego : 5,titulo : 'Kingdom Hearts', fechaLanzamiento : '2002-11-15', categoria : 'Rol de Acción', pegi: 7},
    {idJuego : 6,titulo : 'The Binding Of Isaac', fechaLanzamiento : '2011-09-28', categoria : 'Acción-Aventura', pegi: 16},
    {idJuego : 7,titulo : 'Doom Eternal', fechaLanzamiento : '2020-03-20', categoria : 'FPS', pegi: 18}
  ];