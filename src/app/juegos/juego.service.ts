import { Injectable } from '@angular/core';
import { of, Observable, throwError } from 'rxjs';
import { Juego } from './juego';
import { JUEGOS } from './juegos.json';
import { catchError } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AlertService } from '../alert/alert.service';

@Injectable({
  providedIn: 'root'
})
export class JuegoService {

  private urlServer: string = 'http://localhost:8090/';
  private httpHeaders = new HttpHeaders({'Content-Type': 'application/json'});

  constructor(private http: HttpClient, private alertService: AlertService) { }

  getJuegos() : Observable<Juego[]> {
    //return of(JUEGOS);
    return this.http.get<Juego[]>(`${this.urlServer}juegos`).pipe(
      catchError(error => {
        console.error(`getJuegos error: "${error.message}"`, {})
        this.alertService.error(`Error al consultar los juegos: "${error.message}"`)
        return throwError(error);
      })
    )
    };
  
  
  createJuego(juego: Juego) : Observable<Juego>{
    return this.http.post<Juego>(`${this.urlServer}juegos`, juego,{headers: this.httpHeaders}).pipe(
      catchError(error => {
        console.error(`createJuego error: "${error.message}"`);
        if(error.status == 400){
          error.error.errorMessage.replace('[', '').replace(']','').split(', ').reverse().forEach(errorMessage => {
            this.alertService.error(errorMessage);
          });
        }
        this.alertService.error(`Error al consultar los juegos: "${error.message}"`);
        return throwError(error);
      })
    )
    };

    updateJuego(juego: Juego) : Observable<Juego>{
      return this.http.put<Juego>(`${this.urlServer}juegos/${juego.idJuego}`, juego,{headers: this.httpHeaders}).pipe(
        catchError(error => {
          console.error(`updateJuego error: "${error.message}"`);
          if(error.status == 400){
            error.error.errorMessage.replace('[', '').replace(']','').split(', ').reverse().forEach(errorMessage => {
              this.alertService.error(errorMessage);
            });
            
          }
          this.alertService.error(`Error al actualizar el juegos: "${error.message}"`);
          return throwError(error);
        })
      )
      };

      getJuego(idJuego: number) : Observable<Juego>{
        console.log(`${this.urlServer}juegos/${idJuego}`);

        return this.http.get<Juego>(`${this.urlServer}juegos/${idJuego}`,{headers: this.httpHeaders}).pipe(
          catchError(error => {
            console.error(`getJuego error: "${error.message}"`);
            this.alertService.error(`Error al obtener el juego (${idJuego}): "${error.message}"`);
            return throwError(error);
          })
        )
        };

      deleteJuego(idJuego : number) : Observable<any>{
        return this.http.delete<Juego>(`${this.urlServer}juegos/${idJuego}`).pipe(
          catchError(error => {
            console.error(`deleteJuego error: "${error.message}"`, {})
            this.alertService.error(`Error al borrar el juegos: "${error.message}"`)
            return throwError(error);
          })
        )
        };
  }

