import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {RouterModule, Router, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { ClientesComponent } from './clientes/clientes.component';
import { JuegosComponent } from './juegos/juegos.component';
import { FormsModule, NgModel } from '@angular/forms';
import { ClienteService } from './clientes/cliente.service';
import { AlertComponent } from './alert/alert.component';
import { HttpClientModule } from '@angular/common/http';
import { JuegoFormComponent } from './juegos/form.component';
import { CompaniaComponent } from './compania/compania.component';
import { CompaniaFormComponent } from './compania/form/form.component';


const ROUTES : Routes = [
  {path : '', redirectTo : '/', pathMatch: 'full'},
  {path : 'juegos', component: JuegosComponent},
  {path: 'juegos/form', component: JuegoFormComponent},
  {path: 'juegos/form/:id', component: JuegoFormComponent},
  {path: 'clientes', component: ClientesComponent},
  {path : 'companias', component: CompaniaComponent},
  {path : 'companias/form' ,component : CompaniaFormComponent}
];
@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HeaderComponent,
    ClientesComponent,
    JuegosComponent,
    AlertComponent,
    JuegoFormComponent,
    CompaniaComponent,
    CompaniaFormComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(ROUTES),
    FormsModule
  ],
  providers: [ClienteService],
  bootstrap: [AppComponent]
})
export class AppModule { }
