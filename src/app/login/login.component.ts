import { Component, OnInit } from '@angular/core';
import { LoginService } from './login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: []
})
export class LoginComponent implements OnInit {

  credentials : any;
  constructor( private loginService :LoginService, private router : Router) { }

  ngOnInit(): void {
    this.credentials = {usernam: '', password: ''};
  }
  login(): void{
    console.log(this.credentials);
    this.loginService.save(this.credentials);
    this.router.navigate(['/companies']);
  }
  
}
