import { Injectable } from '@angular/core';
import { CLIENTES } from './clientes.json';
import { Cliente } from './cliente';
import { of, Observable } from 'rxjs';
import { ClientesComponent } from './clientes.component';

@Injectable({
  providedIn: 'root'
})
export class ClienteService {
  

  constructor() { }

  getClientes() : Observable<Cliente[]> {
    return of(CLIENTES);
  }
}
