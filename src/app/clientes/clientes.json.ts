import { Cliente } from "./cliente";

export const CLIENTES: Cliente[] = [
    {idCliente : 1,nombre : 'Pedro', apellido : 'Borges', email : 'pedro@gmail.com', createAt: '2017-12-11'},
    {idCliente : 2,nombre : 'Gabriela', apellido : 'Mayato', email : 'gabriela@gmail.com', createAt: '2017-12-11'},
    {idCliente : 3,nombre : 'Veronica', apellido : 'Polegre', email : 'veronica@gmail.com', createAt: '2017-12-11'},
    {idCliente : 4,nombre : 'Yeray', apellido : 'Escobar', email : 'yeray@gmail.com', createAt: '2017-12-11'},
    {idCliente : 5,nombre : 'Adrian', apellido : 'Herrera', email : 'adrian@gmail.com', createAt: '2017-12-11'},
    {idCliente : 6,nombre : 'Angela', apellido : 'Santamaria', email : 'angela@gmail.com', createAt: '2017-12-11'},
    {idCliente : 7,nombre : 'Jake', apellido : 'Peralta', email : 'jake@gmail.com', createAt: '2017-12-11'}
  ];