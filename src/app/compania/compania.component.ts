import { Component, OnInit } from '@angular/core';
import { Compania } from './compania';
import { CompaniaService } from './compania.service';
import { AlertService } from '../alert/alert.service';

@Component({
  selector: 'app-compania',
  templateUrl: './compania.component.html',
  styleUrls: []
})
export class CompaniaComponent implements OnInit {

  companias : Compania[] = [];
  showId : boolean;

  constructor(private companiaService : CompaniaService, private alertService : AlertService) { }

  ngOnInit(): void {
    this.refreshCompanias();

  }

  switchId() :void {
    this.showId = !this.showId;
  }

  refreshCompanias(): void{
    this.companiaService.getCompanies().subscribe(
      companias => this.companias = companias
    );
  }

  delete(id: number): void{
    if(confirm(`¿Esta seguro que desea elminar la Compania con ID: ${id}`)){
     this.companiaService.deleteCompania(id).subscribe(response => {
      this.alertService.success(`Se ha eliminado correctamente el juego con ID: ${id}`, {autoClose: true});
      this.refreshCompanias();
     });
    }
  }

}
