import { Component, OnInit } from '@angular/core';
import { Compania } from '../compania';
import { CompaniaService } from '../compania.service';
import { AlertService } from '../../alert/alert.service';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-companiaform',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class CompaniaFormComponent implements OnInit {

  compania : Compania = new Compania();
  title : string;

  constructor(private companiaService: CompaniaService, private alertService: AlertService, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.title = 'Crear Compañia';
  }

  create(): void{
    this.companiaService.createCompania(this.compania).subscribe(
      compania => {
        this.alertService.success(`Se ha creado correctamente el juego "${compania.nombre}" con ID: ${compania.idCompania}`)
        this.router.navigate(['/companias']);
    }
    )
  }

  

}
