import { Injectable } from '@angular/core';
import { Compania } from './compania';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AlertService } from '../alert/alert.service';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class CompaniaService {

  private urlServer: string = 'http://localhost:8090/';
  private httpHeaders = new HttpHeaders({'Content-Type': 'application/json'});


  constructor(private http: HttpClient, private alertService: AlertService, private router: Router) { }

  getCompanies(): Observable<Compania[]> {
    return this.http.get<Compania[]>(`${this.urlServer}companies`).pipe(
      catchError(error => {
        console.error(`getCompanies error: "${error.message}"`);
        if (error.status == 401) {
          this.router.navigate(['/login']);
        } else {
          this.alertService.error(`Error al consultar las compañías: "${error.message}"`);
        }
        return throwError(error);
      })
    );
  } 

  createCompania(compania: Compania) : Observable<Compania>{
    return this.http.post<Compania>(`${this.urlServer}companies`, compania,{headers: this.httpHeaders}).pipe(
      catchError(error => {
        console.error(`createCompania error: "${error.message}"`);
        if(error.status == 400){
          error.error.errorMessage.replace('[', '').replace(']','').split(', ').reverse().forEach(errorMessage => {
            this.alertService.error(errorMessage);
          });
        }
        this.alertService.error(`Error al consultar las companias: "${error.message}"`);
        return throwError(error);
      })
    )
    };

  deleteCompania(idCompania : number) : Observable<any>{
    return this.http.delete<Compania>(`${this.urlServer}juegos/${idCompania}`).pipe(
      catchError(error => {
        console.error(`borrarCompania error: "${error.message}"`, {})
        this.alertService.error(`Error al borrar la compania: "${error.message}"`)
        return throwError(error);
      })
    )
    };
}
